# MTMS Spring Boot Security Jwt Authentication

This is a sample application to manage MTMS operations like add,Update,Delete Local Technicians, Experts, Project Managers etc.
This project is build on MSA architecute where several end points are exposed. for example : some endpoints to display list of pending
inspections and other end point to stream video between Local Technician and Expert; More end points can be added based on the requirement.

MTMS application uses GitLab as a repository and also uses CI/CD feature of gitlab. There is a file called gitlab-ci.yml in the root.
There are several build steps are defined for example: build,Unit Test,Creating Image, deploy to UAT/DEV, Deploy to OpenShift.
When the developer commit the code GitLab CI/CD pipelne automatically triggers and read the gitlab-ci.yml and build,unit test and
all build stages are passed.

There is another file DockerFile is placed which is used to create a docker image of the application and deploye to the environment.

We are are using Openshift to handle the deployement. As Openshift works on top of Kubernetes an additional file Kubernetes.yaml is defined
Which create a POD which contains the docker image of the application. The benefit of using Openshift is we can based on the load
can increase and decrease the load.

## Technology Used

 1. Spring Boot (1.5.8.RELEASE)
 2. JWT (0.6.0)
 3. Docker (1.8.0)
 4. Gitlab(11.6.1)
 4. OpenShift
 5. Lombok
 4. Java 1.8
