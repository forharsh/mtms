package com.ycompany.model;

import lombok.Data;

import java.util.List;

@Data
public class LoginUser {

    private String username;
    private String password;
    private List<String> role;

}
