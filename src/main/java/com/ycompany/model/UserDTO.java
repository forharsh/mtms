package com.ycompany.model;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {

    private String userName;
    private List<String> roles;

}
