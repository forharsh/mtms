package com.ycompany.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ScheduledInspectionDTO {

    private String inspectionId;
    private String name;
    private String description;
    private TowerDTO tower;
    private TechnicianDTO assignee;
    private LocalDateTime scheduledDate;
}
