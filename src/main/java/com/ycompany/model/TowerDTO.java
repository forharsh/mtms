package com.ycompany.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TowerDTO {

    private String towerId;
    private TowerLocation location;


}
