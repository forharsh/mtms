package com.ycompany.model;

import lombok.Builder;
import lombok.Data;

/**
 * The Class TowerLocation.
 */

@Data
@Builder
public class TowerLocation {

    private String addressLLine1;
    private String addressLLine2;
    private String addressLLine3;
    private String city;
    private String state;
    private String area;
    private String pincode;

}
