package com.ycompany.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TechnicianDTO {

    private String name;
    private String employeeId;
    private String contactNo;

}
