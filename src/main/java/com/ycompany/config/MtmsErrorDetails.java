package com.ycompany.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MtmsErrorDetails extends Exception{

    private final String message;

}
