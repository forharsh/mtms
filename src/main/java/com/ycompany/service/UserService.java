package com.ycompany.service;

import com.ycompany.model.User;
import com.ycompany.model.UserDTO;

import java.util.List;

public interface UserService {

    UserDTO save(User user);
    List<UserDTO> findAll();
    void delete(String username);

    User findOne(String username);

    UserDTO findById(String username);

    //User update(User user);
}
