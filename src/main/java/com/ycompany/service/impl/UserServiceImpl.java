package com.ycompany.service.impl;

import com.ycompany.repository.UserRepository;
import com.ycompany.model.Roles;
import com.ycompany.model.User;
import com.ycompany.model.UserDTO;
import com.ycompany.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	private final UserRepository userRepository;
	private final BCryptPasswordEncoder bcryptEncoder;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder bcryptEncoder) {
		this.userRepository = userRepository;
		this.bcryptEncoder = bcryptEncoder;
	}

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUserName(username);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for (Roles role : user.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
		}
		return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), grantedAuthorities);
	}

	public List<UserDTO> findAll() {
		List<UserDTO> list = new ArrayList<>();
		Iterable<User> all = userRepository.findAll();
		all.forEach(user -> {
			UserDTO userDTO = new UserDTO();
			userDTO.setUserName(user.getUserName());
			List<String> roles = new ArrayList<>();
			user.getRoles().forEach(role -> {
				roles.add(role.getRole());
			});
			userDTO.setRoles(roles);
			list.add(userDTO);
		});
		return list;
	}

	@Override
	public void delete(String username) {
		userRepository.deleteById(username);
	}

	@Override
	public User findOne(String username) {
		return userRepository.findByUserName(username);
	}

	@Override
	public UserDTO findById(String username) {
		UserDTO userDTO = new UserDTO();
		User byUserName = userRepository.findByUserName(username);
		userDTO.setUserName(byUserName.getUserName());
		List<String> roles = new ArrayList<>();
		byUserName.getRoles().forEach(role -> {
			roles.add(role.getRole());
		});
		userDTO.setRoles(roles);
		return userDTO;
	}

    @Override
    public UserDTO save(User user) {
		user.setPassword(bcryptEncoder.encode(user.getPassword()));
		User saveUser = userRepository.save(user);
		UserDTO userDTO = new UserDTO();
		userDTO.setUserName(saveUser.getUserName());
		List<String> roles = new ArrayList<>();
		saveUser.getRoles().forEach(role -> {
			roles.add(role.getRole());
		});
		userDTO.setRoles(roles);
		return userDTO;
    }
}
