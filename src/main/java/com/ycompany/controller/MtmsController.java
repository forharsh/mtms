package com.ycompany.controller;

import com.ycompany.model.ScheduledInspectionDTO;
import com.ycompany.model.TechnicianDTO;
import com.ycompany.model.TowerDTO;
import com.ycompany.model.TowerLocation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/")
public class MtmsController {

    @RequestMapping(value = "inspection", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<?> inspection()  {
        return new ResponseEntity<>(listScheduledInspections(), HttpStatus.OK);
    }

    /**
     * Service which stream video between local technician and expert.
     * @return
     */
    @RequestMapping(value = "streamvideo")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    // Streaming between Local Technician and Expert.
    public ResponseEntity<?> streamVideo()  {
        return new ResponseEntity<>(listScheduledInspections(), HttpStatus.OK);
    }

    /**
     * List scheduled inspections.
     * @return the list
     */
    private List<ScheduledInspectionDTO> listScheduledInspections() {
        List<ScheduledInspectionDTO> inspectionList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ScheduledInspectionDTO scheduledInspectionDTO = ScheduledInspectionDTO.builder()
                    .inspectionId("Inspection-" + i)
                    .name("Tower-" + i + " Problem")
                    .description("XYZ Fault in the tower-" + i)
                    .assignee(TechnicianDTO.builder()
                            .contactNo("91XXXXXXXX")
                            .employeeId("Tech-" + i)
                            .name("Technician-" + i)
                            .build())
                    .tower(TowerDTO.builder()
                            .location(TowerLocation.builder()
                                    .addressLLine1("Address Line1 -" + i)
                                    .addressLLine2("Address Line2 -" + i)
                                    .addressLLine3("Address Line3 -" + i)
                                    .area("Area - " + i)
                                    .city("city - " + i)
                                    .pincode("12500" + i)
                                    .state("State - " + i)
                                    .build())
                            .towerId("Tower-" + i)
                            .build())
                    .scheduledDate(LocalDateTime.now())
                    .build();
            inspectionList.add(scheduledInspectionDTO);
        }
        return inspectionList;
    }
}
