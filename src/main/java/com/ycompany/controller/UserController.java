package com.ycompany.controller;

import com.ycompany.model.LoginUser;
import com.ycompany.model.Roles;
import com.ycompany.model.User;
import com.ycompany.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody LoginUser loginUser){
        User user = getUser(loginUser);
        return new ResponseEntity<>(userService.save(user),HttpStatus.CREATED);
    }

    private User getUser(@RequestBody LoginUser loginUser) {
        User user = new User();
        user.setUserName(loginUser.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(loginUser.getPassword()));
        Set<Roles> rolesList = new HashSet<>();

        loginUser.getRole().forEach(auth -> {
            Roles role = new Roles();
            role.setUser(user);
            role.setUserName(user.getUserName());
            role.setRole(auth);
            rolesList.add(role);
        });
        user.setRoles(rolesList);
        return user;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable String userName) {
        userService.delete(userName);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @GetMapping
    public ResponseEntity<?> listUser(){
        Collection<? extends GrantedAuthority> authorityList = SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities();
        String havingAuthority = authorityList.stream().map(GrantedAuthority :: getAuthority)
                .collect(Collectors.joining(","));
        return new ResponseEntity<>(userService.findAll(),HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable String id){
        return new ResponseEntity<>(userService.findById(id),HttpStatus.OK);
    }


}
