package com.ycompany;

import com.ycompany.repository.UserRepository;
import com.ycompany.model.Roles;
import com.ycompany.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements ApplicationRunner {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public DataLoader(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        User user = new User();
        user.setUserName("admin");
        user.setPassword(bCryptPasswordEncoder.encode("123"));
        Roles role = new Roles();
        role.setUser(user);
        role.setUserName(user.getUserName());
        role.setRole("ROLE_ADMIN");
        Set<Roles> rolesList = new HashSet<>();
        rolesList.add(role);
        user.setRoles(rolesList);

        userRepository.save(user);

        User user1 = new User();
        user1.setUserName("user");
        user1.setPassword(bCryptPasswordEncoder.encode("123"));
        Roles role1 = new Roles();
        role1.setUser(user1);
        role1.setUserName(user1.getUserName());
        role1.setRole("ROLE_USER");
        Set<Roles> rolesList1 = new HashSet<>();
        rolesList1.add(role1);
        user1.setRoles(rolesList1);

        userRepository.save(user1);
    }
}
