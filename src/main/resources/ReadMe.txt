<h4>API Details</h4>
API Name  - Token Generation
URL - localhost:8080/token/generate-token
Method - POST
Header - Content-Type: application/json
Body -
{
	"username":"user",
	"password":"123"
}

Response :

{
    "status": 200,
    "message": "success",
    "result": {
        "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbGV4MTIzIiwic2NvcGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dLCJpc3MiOiJodHRwOi8vZGV2Z2xhbi5jb20iLCJpYXQiOjE1NDEwNjIzOTMsImV4cCI6MTU0MTA4MDM5M30.DMoB5kv72X7Jf-U5APdjK3UUcGomA9NuJj6XGxmLyqE",
        "username": "user"
    }
}

API Name  - List User
URL - http://localhost:8080/users
Method - Get
Header - Content-Type: application/json
    Authorization : Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbGV4MTIzIiwic2NvcGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dLCJpc3MiOiJodHRwOi8vZGV2Z2xhbi5jb20iLCJpYXQiOjE1NDEwNjIzOTMsImV4cCI6MTU0MTA4MDM5M30.DMoB5kv72X7Jf-U5APdjK3UUcGomA9NuJj6XGxmLyqE
Response -
{
    "status": 200,
    "result": [
                  {
                      "userName": "admin",
                      "roles": [
                          "ROLE_ADMIN"
                      ]
                  },
                  {
                      "userName": "user",
                      "roles": [
                          "ROLE_USER"
                      ]
                  }
              ]
}

API Name  - Create User
URL - http://localhost:8080/users
Method - POST
Header - Content-Type: application/json
    Authorization : Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbGV4MTIzIiwic2NvcGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dLCJpc3MiOiJodHRwOi8vZGV2Z2xhbi5jb20iLCJpYXQiOjE1NDEwNjIzOTMsImV4cCI6MTU0MTA4MDM5M30.DMoB5kv72X7Jf-U5APdjK3UUcGomA9NuJj6XGxmLyqE
Body -
{
	"username" : "harsh",
	"password" : "123",
	"role" : ["ROLE_ADMIN","ROLE_USER"]
}

Response -

{
    "status": 201,
    "message": "User saved successfully.",
    "result": ["ROLE_ADMIN","ROLE_USER"]
}


API Name  - List of Inspections
URL - localhost:8080/api/v1/inspection
Method - GET
Header - Content-Type: application/json
    Authorization : Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbGV4MTIzIiwic2NvcGVzIjpbeyJhdXRob3JpdHkiOiJST0xFX0FETUlOIn1dLCJpc3MiOiJodHRwOi8vZGV2Z2xhbi5jb20iLCJpYXQiOjE1NDEwNjIzOTMsImV4cCI6MTU0MTA4MDM5M30.DMoB5kv72X7Jf-U5APdjK3UUcGomA9NuJj6XGxmLyqE

Response -

{
    "status": 201,
    "message": "User saved successfully.",
    "result": [
                  {
                      "inspectionId": "Inspection-1",
                      "name": "Tower-1 Problem",
                      "description": "XYZ Fault in the tower-1",
                      "tower": {
                          "towerId": "Tower-1",
                          "location": {
                              "addressLLine1": "Address Line1 -1",
                              "addressLLine2": "Address Line2 -1",
                              "addressLLine3": "Address Line3 -1",
                              "city": "city - 1",
                              "state": "State - 1",
                              "area": "Area - 1",
                              "pincode": "125001"
                          }
                      },
                      "assignee": {
                          "name": "Technician-1",
                          "employeeId": "Tech-1",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-2",
                      "name": "Tower-2 Problem",
                      "description": "XYZ Fault in the tower-2",
                      "tower": {
                          "towerId": "Tower-2",
                          "location": {
                              "addressLLine1": "Address Line1 -2",
                              "addressLLine2": "Address Line2 -2",
                              "addressLLine3": "Address Line3 -2",
                              "city": "city - 2",
                              "state": "State - 2",
                              "area": "Area - 2",
                              "pincode": "125002"
                          }
                      },
                      "assignee": {
                          "name": "Technician-2",
                          "employeeId": "Tech-2",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-3",
                      "name": "Tower-3 Problem",
                      "description": "XYZ Fault in the tower-3",
                      "tower": {
                          "towerId": "Tower-3",
                          "location": {
                              "addressLLine1": "Address Line1 -3",
                              "addressLLine2": "Address Line2 -3",
                              "addressLLine3": "Address Line3 -3",
                              "city": "city - 3",
                              "state": "State - 3",
                              "area": "Area - 3",
                              "pincode": "125003"
                          }
                      },
                      "assignee": {
                          "name": "Technician-3",
                          "employeeId": "Tech-3",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-4",
                      "name": "Tower-4 Problem",
                      "description": "XYZ Fault in the tower-4",
                      "tower": {
                          "towerId": "Tower-4",
                          "location": {
                              "addressLLine1": "Address Line1 -4",
                              "addressLLine2": "Address Line2 -4",
                              "addressLLine3": "Address Line3 -4",
                              "city": "city - 4",
                              "state": "State - 4",
                              "area": "Area - 4",
                              "pincode": "125004"
                          }
                      },
                      "assignee": {
                          "name": "Technician-4",
                          "employeeId": "Tech-4",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-5",
                      "name": "Tower-5 Problem",
                      "description": "XYZ Fault in the tower-5",
                      "tower": {
                          "towerId": "Tower-5",
                          "location": {
                              "addressLLine1": "Address Line1 -5",
                              "addressLLine2": "Address Line2 -5",
                              "addressLLine3": "Address Line3 -5",
                              "city": "city - 5",
                              "state": "State - 5",
                              "area": "Area - 5",
                              "pincode": "125005"
                          }
                      },
                      "assignee": {
                          "name": "Technician-5",
                          "employeeId": "Tech-5",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-6",
                      "name": "Tower-6 Problem",
                      "description": "XYZ Fault in the tower-6",
                      "tower": {
                          "towerId": "Tower-6",
                          "location": {
                              "addressLLine1": "Address Line1 -6",
                              "addressLLine2": "Address Line2 -6",
                              "addressLLine3": "Address Line3 -6",
                              "city": "city - 6",
                              "state": "State - 6",
                              "area": "Area - 6",
                              "pincode": "125006"
                          }
                      },
                      "assignee": {
                          "name": "Technician-6",
                          "employeeId": "Tech-6",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-7",
                      "name": "Tower-7 Problem",
                      "description": "XYZ Fault in the tower-7",
                      "tower": {
                          "towerId": "Tower-7",
                          "location": {
                              "addressLLine1": "Address Line1 -7",
                              "addressLLine2": "Address Line2 -7",
                              "addressLLine3": "Address Line3 -7",
                              "city": "city - 7",
                              "state": "State - 7",
                              "area": "Area - 7",
                              "pincode": "125007"
                          }
                      },
                      "assignee": {
                          "name": "Technician-7",
                          "employeeId": "Tech-7",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-8",
                      "name": "Tower-8 Problem",
                      "description": "XYZ Fault in the tower-8",
                      "tower": {
                          "towerId": "Tower-8",
                          "location": {
                              "addressLLine1": "Address Line1 -8",
                              "addressLLine2": "Address Line2 -8",
                              "addressLLine3": "Address Line3 -8",
                              "city": "city - 8",
                              "state": "State - 8",
                              "area": "Area - 8",
                              "pincode": "125008"
                          }
                      },
                      "assignee": {
                          "name": "Technician-8",
                          "employeeId": "Tech-8",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-9",
                      "name": "Tower-9 Problem",
                      "description": "XYZ Fault in the tower-9",
                      "tower": {
                          "towerId": "Tower-9",
                          "location": {
                              "addressLLine1": "Address Line1 -9",
                              "addressLLine2": "Address Line2 -9",
                              "addressLLine3": "Address Line3 -9",
                              "city": "city - 9",
                              "state": "State - 9",
                              "area": "Area - 9",
                              "pincode": "125009"
                          }
                      },
                      "assignee": {
                          "name": "Technician-9",
                          "employeeId": "Tech-9",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  },
                  {
                      "inspectionId": "Inspection-10",
                      "name": "Tower-10 Problem",
                      "description": "XYZ Fault in the tower-10",
                      "tower": {
                          "towerId": "Tower-10",
                          "location": {
                              "addressLLine1": "Address Line1 -10",
                              "addressLLine2": "Address Line2 -10",
                              "addressLLine3": "Address Line3 -10",
                              "city": "city - 10",
                              "state": "State - 10",
                              "area": "Area - 10",
                              "pincode": "1250010"
                          }
                      },
                      "assignee": {
                          "name": "Technician-10",
                          "employeeId": "Tech-10",
                          "contactNo": "91XXXXXXXX"
                      },
                      "scheduledDate": "2019-01-17T16:31:03.386"
                  }
              ]
}


